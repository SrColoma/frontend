

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:teacherresourcesproyect/Tarjeta.dart';
import 'package:teacherresourcesproyect/User.dart';
import 'package:teacherresourcesproyect/landWidget.dart';
import 'package:teacherresourcesproyect/tarjetaWidget.dart';


class Perfil extends StatefulWidget {
  @override
  _PerfilState createState() => _PerfilState();
}

class _PerfilState extends State<Perfil> {
  
  Future<List<UserClass>> _getTarjetas() async{
    var data = await http.get("http://192.168.0.5:3000/api/User");
    var jsonData = json.decode(data.body);

    List<UserClass> lista = [];
    for (var i in jsonData){
      UserClass tjta = UserClass(
        i["foto"],
        i["nombre"],
        i["desc"],
        i["folowers"],
        i["views"],
        i["notebookid"],
        i["mypostid"]
      );
      lista.add(tjta);
    }
    return lista;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize:  Size.fromHeight(200),
          child: AppBar(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(15),
              ),
            ),
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              titlePadding: EdgeInsets.only(top:50),
              title:Column(
                children: <Widget>[
                  Container(
                    width: 75,
                    height: 75,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(
                              "https://i.imgur.com/BoN9kdC.png")
                              )
                    )
                  ),

                  Text("Carl Johnson \"cj\"",textScaleFactor: 0.8,),
                  Text("pandillero de la zona",textScaleFactor: 0.6,),
                  Text("Los Santos US",textScaleFactor: 0.6,),
                ],
              ),
            ),
            backgroundColor: Color(0xffc77800),
            elevation: 50.0,
            //leading: Icon(Icons.arrow_back),
            bottom: TabBar(
              indicatorColor: Colors.yellowAccent,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorWeight: 1,
              isScrollable: true,
              tabs: <Widget>[
                Tab(text: "NoteBook"),
                Tab(text: "My Posts"),
              ],

            ),
          ),

        ),

        body:TabBarView(
          children: <Widget>[
            LandPage(),
            LandPage(),
          ],
        ),

      ),
    );
  }
}

abstract class ListItem {}

/* PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child:Row(
              children: <Widget>[
                Image.network("https://www.nunuoropesa.com/wp-content/uploads/2018/03/profile-img-1-300x300.jpg",
                  height: 100,
                )
              ],
            ),
            
          ), */

/* TabBar(
              indicatorColor: Colors.yellowAccent,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorWeight: 1,
              isScrollable: true,
              tabs: <Widget>[
                Tab(text: "NoteBook"),
                Tab(text: "My Posts"),
              ],

            ), */