class TarjetaClass{
  final String img;
  final String titulo;
  final String desc;
  final int reviews;
  final String autor;

  TarjetaClass(this.img, this.titulo, this.desc, this.reviews,this.autor);
}