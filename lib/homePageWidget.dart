import 'package:flutter/material.dart';

import 'barraWidget.dart';
import 'landWidget.dart';

class TheHome extends StatefulWidget {
  @override
  _TheHomeState createState() => _TheHomeState();
}

class _TheHomeState extends State<TheHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:PreferredSize(
        child:Hero(
          tag: "Barra",
          child:Barra(context),
        ), 
      preferredSize:   Size.fromHeight(60),//Size.fromHeight(60),
      ),
      body:LandPage(),
    );
  }
}