class UserClass{
  final String foto;
  final String nombre;
  final String desc;
  final int folowers;
  final int views;
  final String notebookid;
  final String mypostid;

  UserClass(this.foto,this.nombre, this.desc, this.folowers,this.views,this.notebookid,this.mypostid);
}

/*
foto: String,
    nombre: String,
    desc: String,
    folowers: Number,
    views: Number,
    notebookid: String,
    mypostid: String
    */