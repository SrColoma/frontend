import 'package:flutter/material.dart';
import 'package:teacherresourcesproyect/landWidget.dart';
import "package:teacherresourcesproyect/barraWidget.dart";

import 'ProfileWidget.dart';
import 'homePageWidget.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.light(),
      routes: {
        '/' : (BuildContext context) => TheHome(),
        '/profile': (BuildContext context) => Perfil(),
      },
    );
  }

}
