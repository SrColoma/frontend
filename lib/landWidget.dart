import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:teacherresourcesproyect/Tarjeta.dart';
import 'package:teacherresourcesproyect/tarjetaWidget.dart';


class LandPage extends StatefulWidget {
  @override
  _LandPageState createState() => _LandPageState();
}

class _LandPageState extends State<LandPage> {
  
  Future<List<TarjetaClass>> _getTarjetas() async{
    var data = await http.get("http://192.168.0.5:3000/api/Post");
    var jsonData = json.decode(data.body);
    print(data);

    List<TarjetaClass> lista = [];
    for (var i in jsonData){
      TarjetaClass tjta = TarjetaClass(
        i["img"],
        i["titulo"],
        i["desc"],
        i["reviews"],
        i["autor"],
      );
      
      lista.add(tjta);
    }
    return lista;
  }

  @override
  Widget build(BuildContext context) {
    
    
    return Container(

      child: FutureBuilder(
        future: _getTarjetas(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {

          if(snapshot.data==null){
            return Container(
              child: Center(child: Text("Loadoing..."),),
            );
          }else{
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Tarjeta(
                  img: Image.network(snapshot.data[index].img,height: 50,scale: 0.1,),
                  nombre: Text(snapshot.data[index].titulo),
                  desc: Text(snapshot.data[index].desc),
                );
              },
            );

          }
        },

      ),

    );


  }
}

abstract class ListItem {}