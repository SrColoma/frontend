import 'package:flutter/material.dart';

AppBar Barra(BuildContext context){
  return AppBar(
    centerTitle: true,

    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        bottom: Radius.circular(15),

      ),
    ),
    elevation: 20,
    title: Text("Teacher Resources"),
    backgroundColor: Color(0xffc77800),
    actions: <Widget>[
      IconButton(icon: Icon(Icons.arrow_forward),
        onPressed: (){
          Navigator.of(context).pushNamed('/profile');
        },

      ),
    ],
  );
}
