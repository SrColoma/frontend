import 'package:flutter/material.dart';

class Tarjeta extends StatelessWidget {
  final Text nombre;
  final Text desc;
  final Image img;
  Tarjeta({
    this.nombre,
    this.desc,
    this.img,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      
      height: 225,
      child: GestureDetector(
        child: Card(
          color: Color(0xffffa726),
          elevation: 20,
          margin: EdgeInsets.fromLTRB(25, 10, 25, 0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Container(
            margin: EdgeInsets.all(25),
            child: Column(
              
              children: <Widget>[
                img,
                nombre,
                desc,
              ],
            )
          ),
        ), onTap: () {
         // Navigator.of(context).pushNamed('/');
        },
      ),

    );
  }
}

/* Container(
  color: Colors.pink,
  decoration: new BoxDecoration(
    borderRadius: new BorderRadius.circular(16.0),
    color: Colors.green,
  ),
); */