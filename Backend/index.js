const express = require('express');//maneja el servidor
const morgan = require('morgan');//detecta pedidos
const {mongoose} = require("./database")//coneccion con mongodb
const path = require("path");


const app = express();

//  settings
app.set("port",process.env.PORT || 3000) // tomar el puerto de el entorno o 3000

//  middlewares
app.use(morgan("dev"));
app.use(express.json());

//  routes
app.use("/api/task",require("./routes/task.routes"));
app.use("/api/User",require("./routes/User.routes"));
app.use("/api/Post",require("./routes/Post.routes"));
app.use("/api/NoteBook",require("./routes/NoteBook.routes"));

//static files
app.use(express.static(path.join(__dirname,"public")));

//starting server
app.listen(app.get("port"),()=>{
    console.log(`server on port ${app.get("port")}`);
});