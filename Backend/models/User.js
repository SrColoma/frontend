const mongoose = require("mongoose");
const {Schema} = mongoose;

const UserSchema = new Schema({
    //_id: mongoose.Schema.Types.ObjectId,
    foto: String,
    nombre: String,
    desc: String,
    folowers: Number,
    views: Number,
    notebookid: String,
    mypostid: String
});

module.exports = mongoose.model("User",UserSchema);