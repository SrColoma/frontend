const mongoose = require("mongoose");
const {Schema} = mongoose;

const PostSchema = new Schema({
    //_id: mongoose.Schema.Types.ObjectId,
    img: String,
    titulo: String,
    desc: String,
    reviews: Number,
    autor: String
});

module.exports = mongoose.model("Post",PostSchema);