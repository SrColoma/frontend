const express = require("express");
const router = express.Router();
const faker =require('faker');

const Task = require("..\\models\\task");
const User = require("..\\models\\User");
const Post = require("..\\models\\Post");
const Notebook = require("..\\models\\Notebook");

var conteoUser = 0;
var conteoPost = 0;

router.get("/",async (req,res)=>{
    for(let i=0;i<10;i++){
        await User.create({
            foto: faker.image.avatar(),
            nombre: faker.name.firstName(),
            desc: faker.lorem.lines(),
            folowers: faker.random.number(),
            views: faker.random.number(),
            notebookid: "prueba",
            mypostid: "prueba"
        });
        
    }
    for(let i=0;i<10;i++){
        await Post.create({
            img: faker.image.business(),
            titulo: faker.lorem.lines(),
            desc: faker.lorem.paragraph(),
            reviews: faker.random.number(),
            autor: faker.image.avatar(),
        });
        conteoPost++;
        conteoUser++;
    }
    
    res.json({creado:" "+conteoUser +" existentes"});

});
 
router.get("/ver",async (req,res)=>{
    const us = await User.find();
    const ps = await Post.find();
    res.json({users:us,Post:ps});
});

router.get("/borrarUsers",async (req,res)=>{
    const us = await User.find();
    let i=0
    for(i;i<10;i++){
        await User.deleteOne(us[i]);
        conteoUser--;
    }
    res.json({borrados:" "+conteoUser +" existentes"});
});

router.get("/borrarPost",async (req,res)=>{
    const us = await User.find();
    let i=0
    for(i;i<10;i++){
        await Post.deleteOne(us[i]);
        conteoPost--;
    }
    res.json({borrados:" "+conteoPost +" existentes"});
});

module.exports = router;