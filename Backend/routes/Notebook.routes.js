const express = require("express");
const router = express.Router();
const faker =require('faker');

const Notebook = require("..\\models\\Notebook");

router.get("/",async (req,res)=>{
    const Notebooks = await Notebook.find();
    console.log(Notebooks);
    res.json(Notebooks);
});

module.exports = router;