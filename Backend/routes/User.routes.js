const express = require("express");
const router = express.Router();
const faker =require('faker');

const User = require("..\\models\\User");

router.get("/",async (req,res)=>{
    const Users = await User.find();
    res.json(Users);
});

module.exports = router;