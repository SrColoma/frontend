const express = require("express");
const router = express.Router();
const faker =require('faker');

const Post = require("..\\models\\Post");

router.get("/",async (req,res)=>{
    const Posts = await Post.find();
    res.json(Posts);
});

module.exports = router;